require 'csv_conversion'

RSpec.describe CsvConversion do

  DATA_PATH = './spec/files/data.txt'
  CSV_PATH = DATA_PATH.gsub(/txt$/, 'csv')

  context 'works' do
    let(:data_file) { CsvConversion.new(DATA_PATH)}
    after(:each) { File.delete(CSV_PATH) if File.exist?(CSV_PATH)}

    it 'initializes' do
      expect(data_file.path).to eq(DATA_PATH)
      expect(data_file.updated_file).to eq(nil)
    end

    it '#convert' do
      data_file.convert
      expect(data_file.updated_file).to_not eq(nil)
    end

    it '#save_csv' do
      expect(File.exist?(CSV_PATH)).to be_falsey

      data_file.convert
      data_file.save_csv
      expect(File.exist?(CSV_PATH)).to be_truthy
    end

    it 'executes' do
      expect(data_file.updated_file).to eq(nil)

      data_file.execute
      expect(data_file.updated_file).to_not eq(nil)
      expect(File.exist?(CSV_PATH)).to be_truthy
    end
  end
end