# This script will convert spaced output from emails into
# a valid CSV file for input into Excel or Google Sheets.  
# This will make the output infinitely easier to read.

class CsvConversion

  require 'csv'

  attr_reader :path
  attr_accessor :updated_file

  def initialize(local_path)
    @path = local_path
  end

  def convert
    File.open(path, 'r') do |f|
      @updated_file = []

      f.each_line do |row|
        updated_file << row.split(/\s+/)
      end
    end
  end

  def save_csv
    csv_file_name = path.gsub(/\.txt$/, '.csv')
    CSV.open(csv_file_name, 'w') do |csv|
      updated_file.each do |row|
        csv << row
      end
    end
  end

  def execute
    convert
    save_csv
  end

end
